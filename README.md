This used to be the Ruby on Rails application that powers [status.gitlab.com](https://status.gitlab.com/) but we're switched to a SaaS provider.

This software is MIT Expat licensed.

## API
You can use the default json api by calling: [https://status.gitlab.com/status/api](https://status.gitlab.com/status/api)

### JSON format
```
{
"latency":[{"time":1469104321,"state":0,"value":"0.093852s"},...],
"ssh response time":[{"time":1469104321,"state":0,"value":"0.211936s"},...],
"http project response time":[{"time":1469104321,"state":0,"value":"0.727958s"},...],
"http response time":[{"time":1469104321,"state":0,"value":"0.408527s"},...]
}
```
Every key holds multiple monitor samples, the last sample in the array is the most recent monitor sample.

### JSON attributes
- **time:** epoch timestamp
- **state:** 0 OK, 1 NOT OK
- **value:** text