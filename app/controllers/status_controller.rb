require 'checkmk'
class StatusController < ApplicationController
  include CheckMk
  def index
    @stats = get_checkmk_json
  end
  def api
    @stats = get_checkmk_json
    render json: @stats
  end
end
